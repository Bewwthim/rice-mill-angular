package main

import (
	"bufio"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/handlers"
	"github.com/julienschmidt/httprouter"
)

type Warehouse_mew struct {
	Id      string `json:"id"`
	Details string `json:"details"`
}

type Block struct {
	Block_id   int    `json:"block_id"`
	Block_name string `json:"block_name"`
}

//GetReport
type GetReport struct {
	Id         int    `json:"id"`
	Amount     int    `json:"amount"`
	Block_name string `json:"block_name"`
	Upd_date   string `json:"upd_date"`
	Note_info  string `json:"note_info"`
}

//PostReport
type PostReport struct {
	Id           int    `json:"id"`
	Warehouse_id string `json:"warehouse_id"`
	Amount       int    `json:"amount"`
	Block_id     int    `json:"block_id"`
	Upd_date     string `json:"upd_date"`
	Note_id      int    `json:"note_id"`
}

type Note struct {
	Note_id   int    `json:"note_id"`
	Note_info string `json:"Note_info"`
}

//GetAmount
type GetAmount struct {
	Id         int    `json:"id"`
	Block_id   int    `json:"block_id"`
	Block_name string `json:"block_name"`
	Amount     int    `json:"amount"`
}

//GetCountBlock
type CountBlock struct {
	Warehouse_id   string `json:"warehouse_id"`
	Warehouse_name string `json:"Warehouse_name"`
	NumberOfBlock  string `json:"numberOfBlock"`
}

type GetTotal struct {
	Total int `json:"total"`
}

type Imgpath struct {
	ID       int    `json:"id"`
	Filename string `json:"filename"`
	Filepath string `json:""filepath"`
	Imgdata  string `json:"imgdata"`
	Upd_date string `json:"upd_date"`
}

func Index(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	_, err := w.Write([]byte("Welcome!"))
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func NewWarehouse() *Warehouse_mew {
	return new(Warehouse_mew)
}

func (data *Warehouse_mew) GetAllWarehouses() ([]Warehouse_mew, error) {
	warehouses := make([]Warehouse_mew, 0)
	db, err := GetDB()
	if err != nil {
		return warehouses, err
	}
	defer db.Close()
	rows, err := db.Query(`SELECT id, details FROM warehouse_mew `)
	if err != nil {
		return warehouses, err
	}
	for rows.Next() {
		warehouse := Warehouse_mew{}
		rows.Scan(&warehouse.Id, &warehouse.Details)
		warehouses = append(warehouses, warehouse)
	}
	return warehouses, nil
}

func (data *Warehouse_mew) FindWarehouseByID(ID string) ([]Warehouse_mew, error) {
	warehouses := make([]Warehouse_mew, 0)
	db, err := GetDB()
	if err != nil {
		return warehouses, err
	}
	defer db.Close()
	rows, err := db.Query(`SELECT id, details FROM warehouse_mew WHERE id = ` + ID)
	if err != nil {
		return warehouses, err
	}
	if rows.Next() {
		warehouse := Warehouse_mew{}
		rows.Scan(&warehouse.Id, &warehouse.Details)
		warehouses = append(warehouses, warehouse)
	}
	return warehouses, err
}

func (data *Warehouse_mew) Update(newWarehouse Warehouse_mew) error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	_, err = db.Exec(`UPDATE warehouse SET id = ?, details = ? WHERE id = ?`, newWarehouse.Id, newWarehouse.Details, newWarehouse.Id)
	if err != nil {
		return err
	}
	return nil
}

// FindAllWarehousesreturn all warehouses in database
func GetAllWarehouses(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	users, err := NewWarehouse().GetAllWarehouses()
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(users)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (data *Warehouse_mew) SaveWarehouse() error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	defer db.Close()

	_, err = db.Exec(`INSERT INTO warehouse_mew (id, details) VALUES (?, ?)`, data.Id, data.Details)
	if err != nil {
		return err
	}
	return nil
}

//create a warehouses
func CreateWarehouse(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	body := req.Body
	warehouse := NewWarehouse()
	err := json.NewDecoder(body).Decode(warehouse)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer body.Close()
	err = warehouse.SaveWarehouse()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

// FindWarehouseByID return a warehouse
func FindWarehouseByID(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := params.ByName("id")
	warehouse, err := NewWarehouse().FindWarehouseByID(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(warehouse)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// UpdateWarehouse update an existing warehouse
func UpdateWarehouse(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	ID := params.ByName("id")
	var warehouse Warehouse_mew
	err := json.NewDecoder(req.Body).Decode(&warehouse)
	defer req.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if warehouse.Id == "" {
		http.Error(w, "Please set ID in warehouse information", http.StatusBadRequest)
		return
	}
	_, err = NewWarehouse().FindWarehouseByID(ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = NewWarehouse().Update(warehouse)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func GetWarehouseIDByWarehousename(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := params.ByName("id")
	transfer, err := NewWarehouse().GetWarehouseID(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(transfer)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (data *Warehouse_mew) GetWarehouseID(ID string) (Warehouse_mew, error) {
	if ID == "" {
		return Warehouse_mew{}, errors.New("ID can not be empty")
	}
	db, err := GetDB()
	transfer := Warehouse_mew{}
	if err != nil {
		return transfer, err
	}
	defer db.Close()
	rows, err := db.Query(`SELECT id,details FROM warehouse_mew WHERE details = ` + ID)
	if err != nil {
		return transfer, err
	}
	for rows.Next() {
		transfer := Warehouse_mew{}
		rows.Scan(&transfer.Id, &transfer.Details)
		return transfer, err
	}
	return Warehouse_mew{}, err
}

func NewBlock() *Block {
	return new(Block)
}

// call getBlock()
func GetBlockByWarehouseID(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := params.ByName("id")
	block, err := NewBlock().GetBlockID(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(block)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (data *Block) GetBlockID(ID string) ([]Block, error) {
	blocks := make([]Block, 0)
	db, err := GetDB()
	if err != nil {
		return blocks, err
	}
	defer db.Close()
	rows, err := db.Query(`call getBlock(` + ID + `)`)
	if err != nil {
		return blocks, err
	}
	for rows.Next() {
		block := Block{}
		rows.Scan(&block.Block_id, &block.Block_name)
		blocks = append(blocks, block)
	}
	return blocks, err
}

func NewReport() *GetReport {
	return new(GetReport)
}

// call getReport()
func GetReportsByWarehouseID(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := params.ByName("id")
	report, err := NewReport().GetReportID(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(report)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

//call  getReport('ก1');
func (data *GetReport) GetReportID(ID string) ([]GetReport, error) {
	reports := make([]GetReport, 0)
	db, err := GetDB()
	if err != nil {
		return reports, err
	}
	defer db.Close()
	rows, err := db.Query(`call  getReport(` + ID + `)`)
	if err != nil {
		return reports, err
	}
	for rows.Next() {
		report := GetReport{}
		rows.Scan(&report.Id, &report.Amount, &report.Block_name, &report.Upd_date, &report.Note_info)
		reports = append(reports, report)
	}
	return reports, err
}

func NewNotes() *Note {
	return new(Note)
}

func (data *Note) GetAllNotes() ([]Note, error) {
	notes := make([]Note, 0)
	db, err := GetDB()
	if err != nil {
		return notes, err
	}
	defer db.Close()
	rows, err := db.Query(`SELECT note_id, note_info FROM note`)
	if err != nil {
		return notes, err
	}
	for rows.Next() {
		note := Note{}
		rows.Scan(&note.Note_id, &note.Note_info)
		notes = append(notes, note)
	}
	return notes, nil
}

func GetNotes(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	users, err := NewNotes().GetAllNotes()
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(users)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func NewPostReport() *PostReport {
	return new(PostReport)
}

func CreateReport(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	body := req.Body
	report := NewPostReport()
	err := json.NewDecoder(body).Decode(report)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer body.Close()
	err = report.CreateNewReport()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (data *PostReport) CreateNewReport() error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	defer db.Close()

	_, err = db.Exec(`INSERT INTO all_report VALUES (NULL,?,?,?,SYSDATE(),?)`, data.Warehouse_id, data.Amount, data.Block_id, data.Note_id)
	if err != nil {
		return err
	}
	return nil
}

func (data *PostReport) DeleteReport(ID string) error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	_, err = db.Exec(`DELETE FROM all_report WHERE id = ?`, data.Id)
	if err != nil {
		return err
	}
	return nil
}

func DeleteReport(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	ID := prm.ByName("id")
	report, err := NewPostReport().FindReportByIDFunc(ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = report.DeleteReport(ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (data *PostReport) FindReportByIDFunc(ID string) (PostReport, error) {
	if ID == "" {
		return PostReport{}, errors.New("ID can not be empty")
	}
	db, err := GetDB()
	report := PostReport{}
	if err != nil {
		return report, err
	}
	defer db.Close()
	rows, err := db.Query(`SELECT id, warehouse_id, amount, block_id , upd_date, note_id FROM all_report WHERE id = ` + ID)
	if err != nil {
		return report, err
	}
	if rows.Next() {
		rows.Scan(&report.Id, &report.Warehouse_id, &report.Amount, &report.Block_id, &report.Upd_date, &report.Note_id)
		return report, nil
	}
	return PostReport{}, err
}

func FindReportByID(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := params.ByName("id")
	report, err := NewPostReport().FindReportByIDFunc(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(report)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func NewGetAmount() *GetAmount {
	return new(GetAmount)
}

func (data *GetAmount) GetAllAmount(ID string) ([]GetAmount, error) {
	amounts := make([]GetAmount, 0)
	db, err := GetDB()
	if err != nil {
		return amounts, err
	}
	defer db.Close()
	rows, err := db.Query(`call getRecord(` + ID + `)`)
	if err != nil {
		return amounts, err
	}
	for rows.Next() {
		amount := GetAmount{}
		rows.Scan(&amount.Id, &amount.Block_id, &amount.Block_name, &amount.Amount)
		amounts = append(amounts, amount)
	}
	return amounts, nil
}

func GetAmountbyWarehouse(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := params.ByName("id")
	users, err := NewGetAmount().GetAllAmount(id)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(users)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func NewCountBlock() *CountBlock {
	return new(CountBlock)
}

func (data *CountBlock) GetCountBlockByWarehouseID() ([]CountBlock, error) {
	blocks := make([]CountBlock, 0)
	db, err := GetDB()
	if err != nil {
		return blocks, err
	}
	defer db.Close()
	rows, err := db.Query(`call getCountBlock()`)
	if err != nil {
		return blocks, err
	}
	for rows.Next() {
		block := CountBlock{}
		rows.Scan(&block.Warehouse_id, &block.Warehouse_name, &block.NumberOfBlock)
		blocks = append(blocks, block)
	}
	return blocks, nil
}

func GetCountBlock(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	users, err := NewCountBlock().GetCountBlockByWarehouseID()
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(users)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func InsertBlock(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	body := req.Body
	id := params.ByName("id")
	block := NewBlock()
	err := json.NewDecoder(body).Decode(block)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer body.Close()
	err = block.CreateNewBlock(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (data *Block) CreateNewBlock(ID string) error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	defer db.Close()

	_, err = db.Exec(`call insertBlock(`+ID+`,`+`?)`, data.Block_name)
	if err != nil {
		return err
	}
	return nil
}

func DeleteBlock(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	warehouse := prm.ByName("id")
	ID := prm.ByName("block")
	block, err := NewBlock().FindBlockByWarehouseID(warehouse)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = block.DeleteBlockFunc(warehouse, ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (data *Block) FindBlockByWarehouseID(warehouse string) (Block, error) {
	if warehouse == "" {
		return Block{}, errors.New("ID can not be empty")
	}
	db, err := GetDB()
	block := Block{}
	if err != nil {
		return block, err
	}
	defer db.Close()
	rows, err := db.Query(`call getBlock(` + warehouse + `)`)
	if err != nil {
		return block, err
	}
	if rows.Next() {
		rows.Scan(&block.Block_id, &block.Block_name)
		return block, nil
	}
	return Block{}, err
}

func (data *Block) DeleteBlockFunc(warehouse string, ID string) error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	_, err = db.Exec(`call deleteBlock(`+warehouse+`,`+`?)`, ID)
	if err != nil {
		return err
	}
	return nil
}

//call getTotal("ก7ท2");
func NewTotal() *GetTotal {
	return new(GetTotal)
}

func (data *GetTotal) GetTotalByWarehouseID(ID string) ([]GetTotal, error) {
	amounts := make([]GetTotal, 0)
	db, err := GetDB()
	if err != nil {
		return amounts, err
	}
	defer db.Close()
	rows, err := db.Query(`call getTotal(` + ID + `)`)
	if err != nil {
		return amounts, err
	}
	for rows.Next() {
		amount := GetTotal{}
		rows.Scan(&amount.Total)
		amounts = append(amounts, amount)
	}
	return amounts, nil
}

func GetTotalByWarehouseID(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := params.ByName("id")
	users, err := NewTotal().GetTotalByWarehouseID(id)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(users)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func NewGrafana() *GrafanaTable {
	return new(GrafanaTable)
}

func GetGrafanaByWarehouseID(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := params.ByName("id")
	users, err := NewGrafana().GetGrafana(id)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(users)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

type GrafanaTable struct {
	Id           int    `json:"id"`
	Warehouse_id string `json:"warehouse_id"`
	Grafana_url  string `json:"grafana"`
	Detail       string `json:"detail"`
}

func (data *GrafanaTable) GetGrafana(ID string) ([]GrafanaTable, error) {
	amounts := make([]GrafanaTable, 0)
	db, err := GetDB()
	if err != nil {
		return amounts, err
	}
	defer db.Close()
	rows, err := db.Query(`SELECT id, warehouse_id, grafana_url, detail FROM warehouse_grafana WHERE warehouse_id = ` + ID)
	if err != nil {
		return amounts, err
	}
	for rows.Next() {
		amount := GrafanaTable{}
		rows.Scan(&amount.Id, &amount.Warehouse_id, &amount.Grafana_url, &amount.Detail)
		amounts = append(amounts, amount)
	}
	return amounts, nil
}

func UploadFile(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	//1. parse input, type multipart/form-data.
	req.ParseMultipartForm(10 << 20)
	//2. retrieve file from posted form-data
	file, handler, err := req.FormFile("myFile")
	if err != nil {
		fmt.Println("Error Retrieve file from form form-data")
		fmt.Println(err)
		return
	}
	defer file.Close()
	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)

	// Create file
	dst, err := os.Create(filepath.Join("C:/go-workspace/GitLab/rice-mill-go/rice_mill/temp-images", filepath.Base(handler.Filename)))
	if err != nil {
		fmt.Println(err)
		return
	}
	defer dst.Close()
	if _, err = io.Copy(dst, file); err != nil {
		fmt.Println(err)
		return
	}

	// Copy the uploaded file to the created file on the filesystem
	if _, err := io.Copy(dst, file); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// create a new buffer base on file size
	fInfo, _ := dst.Stat()
	var size int64 = fInfo.Size()
	buf := make([]byte, size)

	// read file content into buffer
	fReader := bufio.NewReader(dst)
	fReader.Read(buf)
	//convert the buffer bytes to base64 string - use buf.Bytes() for new image
	imgBase64Str := base64.StdEncoding.EncodeToString(buf)

	//Decoding
	sDec, _ := base64.StdEncoding.DecodeString(imgBase64Str)
	fmt.Println(sDec)
	filepat := `\rice_mill\temp-images\` + handler.Filename
	fmt.Println(filepat)
	db, err := GetDB()
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	insert, err := db.Query("INSERT INTO img VALUES (NULL,?,?,?,SYSDATE())", handler.Filename, filepat, imgBase64Str)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()
}

type GetImgfilepath struct {
	Filepath string `json:"filepath"`
}

func NewGetImgfilepath() *GetImgfilepath {
	return new(GetImgfilepath)
}

func Getimg(w http.ResponseWriter, r *http.Request, prm httprouter.Params) {
	var imagepath GetImgfilepath
	if (*r).Method == "OPTIONS" {
		return
	}
	w.Header().Set("Content-Type", "application/json")
	ID := prm.ByName("id")
	db, err := GetDB()
	row, err := db.Query("select filepath from img where id=?", ID)
	if err != nil {
		panic(err.Error())
	} else {
		for row.Next() {
			var imgpath string
			err2 := row.Scan(&imgpath)
			row.Columns()
			if err2 != nil {
				panic(err2.Error())
			} else {
				imgpath := GetImgfilepath{
					Filepath: imgpath,
				}
				imagepath = imgpath
			}
		}
	}
	defer row.Close()
	json.NewEncoder(w).Encode(imagepath)

	// Read the entire file into a byte slice
	// bytes, err := ioutil.ReadFile("C:/go-workspace/GitLab/rice-mill-go/rice_mill/temp-images/IMG_1495.jpg")
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// var base64Encoding string

	// // Determine the content type of the image file
	// mimeType := http.DetectContentType(bytes)

	// // Prepend the appropriate URI scheme header depending
	// // on the MIME type
	// switch mimeType {
	// case "image/jpeg":
	// 	base64Encoding += "data:image/jpeg;base64,"
	// case "image/png":
	// 	base64Encoding += "data:image/png;base64,"
	// }

	// // Append the base64 encoded output
	// base64Encoding += toBase64(bytes)

	// // Print the full base64 representation of the image

	// return base64Encoding
}

func toBase64(b []byte) string {
	return base64.StdEncoding.EncodeToString(b)
}

func NewProduct() *GetProduct {
	return new(GetProduct)
}

func GetProductbyWarehouseID(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := params.ByName("id")
	users, err := NewProduct().Getproducts(id)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(users)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

type GetProduct struct {
	Rice_id    string `json:"rice_id"`
	Rice_name  string `json:"rice_name"`
	Previous   int    `json:"previous"`
	Receive    int    `json:"receive"`
	Packed     int    `json:"packed"`
	MoveIn     int    `json:"move_in"`
	MoveOut    int    `json:"move_out"`
	RiceSale   int    `json:"rice_sale"`
	RiceReturn int    `json:"rice_return"`
	Total      int    `json:"total"`
}

func (data *GetProduct) Getproducts(ID string) ([]GetProduct, error) {
	amounts := make([]GetProduct, 0)
	db, err := GetDB()
	if err != nil {
		return amounts, err
	}
	defer db.Close()
	rows, err := db.Query(`call getNameRiceByWarehouseID(` + ID + `)`)
	if err != nil {
		return amounts, err
	}
	for rows.Next() {
		amount := GetProduct{}
		rows.Scan(&amount.Rice_id, &amount.Rice_name, &amount.Previous, &amount.Receive, &amount.Packed, &amount.MoveIn, &amount.MoveOut, &amount.RiceSale, &amount.RiceReturn, &amount.Total)
		amounts = append(amounts, amount)
	}
	return amounts, nil
}

type AllRecordTable struct {
	Warehouse_id   string `json:"warehouse_id"`
	Warehouse_name string `json:"warehouse_name"`
	Sum_jumpo      int    `json:"sum_jumpo"`
	Sum_bag        int    `json:"sum_bag"`
}

//call getAllRecord()
func NewAllRecord() *AllRecordTable {
	return new(AllRecordTable)
}

func (data *AllRecordTable) GetAllRecords() ([]AllRecordTable, error) {
	records := make([]AllRecordTable, 0)
	db, err := GetDB()
	if err != nil {
		return records, err
	}
	defer db.Close()
	rows, err := db.Query(`call getAllRecord()`)
	if err != nil {
		return records, err
	}
	for rows.Next() {
		record := AllRecordTable{}
		rows.Scan(&record.Warehouse_id, &record.Warehouse_name, &record.Sum_jumpo, &record.Sum_bag)
		records = append(records, record)
	}
	return records, nil
}

func GetAllRecord(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	users, err := NewAllRecord().GetAllRecords()
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(users)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func NewPaddy() *GetPaddy {
	return new(GetPaddy)
}

func GetPaddybyWarehouseID(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := params.ByName("id")
	users, err := NewPaddy().Getpaddies(id)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(users)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

type GetPaddy struct {
	Rice_id         string  `json:"rice_id"`
	Rice_name       string  `json:"rice_name"`
	Amount_previous float64 `json:"amount_previous"`
	Sum_paddy       float64 `json:"sum_paddy"`
	Sum_used        float64 `json:"sum_used"`
	Total           float64 `json:"total"`
}

func (data *GetPaddy) Getpaddies(ID string) ([]GetPaddy, error) {
	amounts := make([]GetPaddy, 0)
	db, err := GetDB()
	if err != nil {
		return amounts, err
	}
	defer db.Close()
	rows, err := db.Query(`call getPaddyByWarehouseID(` + ID + `)`)
	fmt.Println(ID)
	if err != nil {
		return amounts, err
	}
	for rows.Next() {
		amount := GetPaddy{}
		rows.Scan(&amount.Rice_id, &amount.Rice_name, &amount.Amount_previous, &amount.Sum_paddy, &amount.Sum_used, &amount.Total)
		amounts = append(amounts, amount)
	}
	return amounts, nil
}

// NewRouter return all router
func NewRouter() *httprouter.Router {
	router := httprouter.New()
	router.GET("/", Index)
	//Warehouse **
	router.GET("/warehouses", GetAllWarehouses)
	router.GET("/warehouses/:id", FindWarehouseByID)
	//*****************************************************************
	//GetWarehouseID from name **
	router.GET("/warehouseid/:id", GetWarehouseIDByWarehousename)
	//GetBlock from WarehouseID ****
	router.GET("/blocks/:id", GetBlockByWarehouseID)
	//AllReport from WarehouseID ****
	router.GET("/reports/:id", GetReportsByWarehouseID)
	//Create a report ****
	router.POST("/reports", CreateReport)
	//Delete Report by Id ****
	router.DELETE("/reports/:id", DeleteReport)
	//Get a report by id ****
	router.GET("/report/:id", FindReportByID)
	//GetNotes ****
	router.GET("/notes", GetNotes)
	//GetAmount **** หน้าแสดงจำนวนข้าวที่เหลือในบล็อค :id = warehouse id
	router.GET("/amount/:id", GetAmountbyWarehouse)
	//GetCountBlock **** หน้าแสดงจำนวนบล็อคที่มีในโกดัง/โรงสี :id = warehouse id
	router.GET("/getcountblock", GetCountBlock)
	//Insert a block **** หน้าเพิ่ม block ตาม :id = warehouse id
	router.POST("/insertblock/:id", InsertBlock)
	//Delete a block by Id **** หน้าลบ block ตาม :id = warehouse id , ลบ :block = block id นี้
	router.DELETE("/deleteblock/:id/:block", DeleteBlock)
	//Upload picture into temp-images folder
	router.POST("/upload", UploadFile)
	//Upload picture into temp-images folder
	router.GET("/getimg/:id", Getimg)
	//GetTotal **** แสดงจำนวนข้าวในแต่ล่ะโกดัง :id = warehouse id
	router.GET("/total/:id", GetTotalByWarehouseID)
	//GetLinkGrafana
	router.GET("/grafana/:id", GetGrafanaByWarehouseID)
	//Get ผลผลิต
	router.GET("/product/:id", GetProductbyWarehouseID)
	//Get Amount All หน้า dashboard
	router.GET("/allrecord", GetAllRecord)
	//Get ข้าวเปลือก
	router.GET("/paddy/:id", GetPaddybyWarehouseID)
	//*****************************************************************
	// router.POST("/warehouses", CreateWarehouse)
	// router.PUT("/warehouses/:id", UpdateWarehouse)
	// router.DELETE("/warehouses/:id", DeleteWarehouse)
	//Get TransferRice + Packing + Sale
	// router.GET("/transferinfo/:id", FindTransferRiceByID)
	//TransferRice
	// router.POST("/transfers", CreateTransfer)
	// router.GET("/transfers/:id", GetTransferByID)
	// router.DELETE("/transfers/:id", DeleteTransfer)
	// router.PUT("/transfers/:id", UpdateTransfer)
	//Packing
	// router.POST("/packing", CreatePacking)
	// router.GET("/packing/:id", GetPackingByID)
	// router.DELETE("/packing/:id", DeletePacking)
	// router.PUT("/packing/:id", UpdatePacking)
	//Sale
	// router.POST("/sale", CreateRiceSale)
	// router.GET("/sale/:id", GetSaleByID)
	// router.DELETE("/sale/:id", DeleteSale)
	// router.PUT("/sale/:id", UpdateSale)
	//RiceInfo

	router.GET("/ricename", GetAllRicenames)
	router.GET("/ricename/:id", FindRicenamesByID)
	//GetRiceID from name
	router.GET("/riceid/:id", GetRiceIDByRicename)

	return router
}

const (
	DBDriver   = "mysql"
	DBName     = "rice_account"
	DBUser     = "root"
	DBPassword = "test"
	DBURL      = DBUser + ":" + DBPassword + "@tcp(127.0.0.1:3306)/" + DBName
)

func GetDB() (*sql.DB, error) {
	db, err := sql.Open(DBDriver, DBURL)
	if err != nil {
		return db, err
	}
	err = db.Ping()
	if err != nil {
		return db, err
	}
	return db, nil
}

func main() {
	log.Println("Server is up on 9000 port")
	router := NewRouter()
	headersOk := handlers.AllowedHeaders([]string{"Content-Type", "Accept", "Content-Length", "Authorization"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"OPTIONS", "DELETE", "GET", "HEAD", "POST", "PUT", "PATCH"})
	log.Fatalln(http.ListenAndServe(":9000", handlers.CORS(originsOk, headersOk, methodsOk)(router)))

}

//***************************************************************************
type Rice_transfer_record struct {
	Id             int    `json:"id"`
	Upd_date       string `json:"upd_date"`
	RiceName       string `json:"rice_name"`
	Amount         int    `json:"amount"`
	Status         string `json:"status"`
	From_warehouse string `json:"from_warehouse"`
	To_warehouse   string `json:"to_warehouse"`
	Rice_id_from   string `json:"rice_id_from"`
	Rice_id_to     string `json:"rice_id_to"`
}

type Rice_info struct {
	Rice_id   string `json:"rice_id"`
	Rice_name string `json:"rice_name"`
}

type Rice_result struct {
	Id             int    `json:"id"`
	Update_date    string `json:"update_date"`
	RiceName       string `json:"rice_name"`
	Weight         int    `json:"weight"`
	Well           int    `json:"well"`
	Lose           int    `json:"lose"`
	Image_location string `json:"image_location"`
}

type Rice_packing_daily struct {
	Id           int    `json:"id"`
	Warehouse_id string `json:"warehouse_id"`
	Rice_id      string `json:"rice_id"`
	Amount       int    `json:"amount"`
	Upd_date     string `json:"upd_date"`
	Activity     string `json:"activity"`
}

type Rice_sale struct {
	Id           int    `json:"id"`
	Warehouse_id string `json:"warehouse_id"`
	Rice_id      string `json:"rice_id"`
	Amount       int    `json:"amount"`
	Upd_date     string `json:"upd_date"`
}

//For Get
type TransferRice struct {
	Id             int    `json:"id"`
	Upd_date       string `json:"upd_date"`
	RiceName       string `json:"rice_name"`
	Amount         int    `json:"amount"`
	Status         string `json:"status"`
	From_warehouse string `json:"from_warehouse"`
	To_warehouse   string `json:"to_warehouse"`
}

//***************************************************************************

func NewRicenames() *Rice_info {
	return new(Rice_info)
}

func (data *Rice_info) GetAllRice() ([]Rice_info, error) {
	rices := make([]Rice_info, 0)
	db, err := GetDB()
	if err != nil {
		return rices, err
	}
	defer db.Close()
	rows, err := db.Query(`SELECT rice_id, rice_name FROM rice_info WHERE rice_unit="จัมโบ" order by rice_info_id desc LIMIT 20;`)
	if err != nil {
		return rices, err
	}
	for rows.Next() {
		rice := Rice_info{}
		rows.Scan(&rice.Rice_id, &rice.Rice_name)
		rices = append(rices, rice)
	}
	return rices, nil
}

func GetAllRicenames(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	users, err := NewRicenames().GetAllRice()
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(users)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func FindRicenamesByID(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := params.ByName("id")
	rice, err := NewRicenames().FindRicenameByID(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(rice)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (data *Rice_info) FindRicenameByID(ID string) (Rice_info, error) {
	if ID == "" {
		return Rice_info{}, errors.New("ID can not be empty")
	}
	db, err := GetDB()
	rice := Rice_info{}
	if err != nil {
		return rice, err
	}
	defer db.Close()
	rows, err := db.Query(`SELECT rice_id, Rice_name FROM rice_info WHERE rice_id = ` + ID)
	if err != nil {
		return rice, err
	}
	if rows.Next() {
		rows.Scan(&rice.Rice_id, &rice.Rice_name)
		return rice, nil
	}
	return Rice_info{}, err
}

func GetRiceIDByRicename(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := params.ByName("id")
	transfer, err := NewRicenames().FindRiceIDByRicename(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(transfer)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (data *Rice_info) FindRiceIDByRicename(ID string) (Rice_info, error) {
	if ID == "" {
		return Rice_info{}, errors.New("ID can not be empty")
	}
	db, err := GetDB()
	transfer := Rice_info{}
	if err != nil {
		return transfer, err
	}
	defer db.Close()
	rows, err := db.Query(`SELECT rice_id,rice_name FROM rice_info WHERE rice_name = ` + ID)
	if err != nil {
		return transfer, err
	}
	for rows.Next() {
		transfer := Rice_info{}
		rows.Scan(&transfer.Rice_id, &transfer.Rice_name)
		return transfer, err
	}
	return Rice_info{}, err
}

//***************************************************************************

func NewTransfer() *TransferRice {
	return new(TransferRice)
}

// FindTransferRiceByID return a transferRice
func FindTransferRiceByID(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := params.ByName("id")
	transferRice, err := NewTransfer().FindTransferRiceByID(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(transferRice)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (data *TransferRice) FindTransferRiceByID(ID string) ([]TransferRice, error) {
	transferRices := make([]TransferRice, 0)
	db, err := GetDB()
	if err != nil {
		return transferRices, err
	}
	if ID == "" {
		return transferRices, errors.New("ID can not be empty")
	}

	defer db.Close()
	rows, err := db.Query(`SELECT ID, Update_date, RiceName, Amount, Status, From_warehouse_name, To_warehouse_name 
	FROM (SELECT rf.id AS ID, rf.upd_date AS Update_date, ri.rice_name AS RiceName, rf.amount AS Amount, "โยกย้าย" AS Status, w2.details AS From_warehouse_name, w1.details AS To_warehouse_name 
	FROM rice_transfer_record rf 
	JOIN warehouse w1 ON w1.id = rf.to_warehouse 
	JOIN warehouse w2 ON w2.id = rf.from_warehouse 
	JOIN rice_info ri ON rf.rice_id_from = ri.rice_id 
	WHERE w1.id = ` + ID + ` OR w2.id = ` + ID +
		`UNION
	SELECT rp.id AS ID ,rp.upd_date AS Update_date, ri.rice_name AS RiceName, rp.amount AS Amount, "การผลิต" AS Status, "-" AS From_warehouse_name, "-" AS To_warehouse_name 
	FROM rice_packing_daily rp 
	JOIN rice_info ri ON rp.rice_id = ri.rice_id WHERE rp.warehouse_id = ` + ID + ` AND activity = "packed"
	UNION
	SELECT rs.id AS ID ,rs.upd_date AS Update_date, ri.rice_name AS RiceName, rs.amount AS Amount, "การขายออก" AS Status, "-" AS From_warehouse_name, "-" AS To_warehouse_name 
	FROM rice_sale rs 
	JOIN rice_info ri ON rs.rice_id = ri.rice_id WHERE rs.warehouse_id = ` + ID + `) AS result ORDER BY Update_date DESC LIMIT 10`)
	if err != nil {
		return transferRices, err
	}
	for rows.Next() {
		transferRice := TransferRice{}
		rows.Scan(&transferRice.Id, &transferRice.Upd_date, &transferRice.RiceName, &transferRice.Amount, &transferRice.Status, &transferRice.From_warehouse, &transferRice.To_warehouse)
		transferRices = append(transferRices, transferRice)
	}
	return transferRices, err
}

func NewTransferRices() *Rice_transfer_record {
	return new(Rice_transfer_record)
}

func (data *Rice_transfer_record) SaveRiceTransfer() error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	defer db.Close()

	_, err = db.Exec(`INSERT INTO rice_transfer_record VALUES (NULL,SYSDATE(),?,?,?,?,?)`, data.From_warehouse, data.To_warehouse, data.Amount, data.Rice_id_from, data.Rice_id_to)
	if err != nil {
		return err
	}
	return nil
}

func CreateTransfer(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	body := req.Body
	transferRice := NewTransferRices()
	err := json.NewDecoder(body).Decode(transferRice)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer body.Close()
	err = transferRice.SaveRiceTransfer()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func NewPackingDaily() *Rice_packing_daily {
	return new(Rice_packing_daily)
}

func (data *Rice_packing_daily) SaveRicePackingDaily() error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	defer db.Close()

	_, err = db.Exec(`INSERT INTO rice_packing_daily VALUES (NULL,?,?,?,SYSDATE(),"packed")`, data.Warehouse_id, data.Rice_id, data.Amount)
	if err != nil {
		return err
	}
	return nil
}

func CreatePacking(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	body := req.Body
	packing := NewPackingDaily()
	err := json.NewDecoder(body).Decode(packing)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer body.Close()
	err = packing.SaveRicePackingDaily()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func NewRiceSale() *Rice_sale {
	return new(Rice_sale)
}

func (data *Rice_sale) SaveRiceSale() error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	defer db.Close()

	_, err = db.Exec(`INSERT INTO rice_sale VALUES (NULL,?,?,?,SYSDATE())`, data.Warehouse_id, data.Rice_id, data.Amount)
	if err != nil {
		return err
	}
	return nil
}

func CreateRiceSale(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	body := req.Body
	sale := NewRiceSale()
	err := json.NewDecoder(body).Decode(sale)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer body.Close()
	err = sale.SaveRiceSale()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func GetTransferByID(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := params.ByName("id")
	transfer, err := NewTransferRices().FindTransferByID(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(transfer)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (data *Rice_transfer_record) FindTransferByID(ID string) (Rice_transfer_record, error) {
	if ID == "" {
		return Rice_transfer_record{}, errors.New("ID can not be empty")
	}
	db, err := GetDB()
	transfer := Rice_transfer_record{}
	if err != nil {
		return transfer, err
	}
	defer db.Close()
	rows, err := db.Query(`SELECT id, upd_date, from_warehouse, to_warehouse, amount, rice_id_from, rice_id_to FROM rice_transfer_record WHERE id = ` + ID)
	if err != nil {
		return transfer, err
	}
	for rows.Next() {
		transfer := Rice_transfer_record{}
		rows.Scan(&transfer.Id, &transfer.Upd_date, &transfer.From_warehouse, &transfer.To_warehouse, &transfer.Amount, &transfer.Rice_id_from, &transfer.Rice_id_to)
		return transfer, err
	}
	return Rice_transfer_record{}, err
}

func (data *Rice_transfer_record) DeleteRiceTransfer(ID string) error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	_, err = db.Exec(`DELETE FROM rice_transfer_record WHERE id = ?`, data.Id)
	if err != nil {
		return err
	}
	return nil
}

func DeleteTransfer(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	ID := prm.ByName("id")
	transfer, err := NewTransferRices().FindTransferByID(ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = transfer.DeleteRiceTransfer(ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (data *Rice_transfer_record) UpdateTransfers(newRiceTransfer Rice_transfer_record) error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	_, err = db.Exec(`UPDATE rice_transfer_record SET upd_date = SYSDATE(), from_warehouse = ?, to_warehouse = ?, amount = ?, rice_id_from = ? WHERE id = ?`, newRiceTransfer.From_warehouse, newRiceTransfer.To_warehouse, newRiceTransfer.Amount, newRiceTransfer.Rice_id_from, newRiceTransfer.Id)
	if err != nil {
		return err
	}
	return nil
}

func UpdateTransfer(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	ID := params.ByName("id")
	var riceTransfer Rice_transfer_record
	err := json.NewDecoder(req.Body).Decode(&riceTransfer)
	defer req.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if riceTransfer.Id == 0 {
		http.Error(w, "Please set ID in transfer information", http.StatusBadRequest)
		return
	}
	_, err = NewTransferRices().FindTransferByID(ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = NewTransferRices().UpdateTransfers(riceTransfer)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func GetPackingByID(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := params.ByName("id")
	packing, err := NewPackingDaily().FindPackingByID(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(packing)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (data *Rice_packing_daily) FindPackingByID(ID string) (Rice_packing_daily, error) {
	if ID == "" {
		return Rice_packing_daily{}, errors.New("ID can not be empty")
	}
	db, err := GetDB()
	packing := Rice_packing_daily{}
	if err != nil {
		return packing, err
	}
	defer db.Close()
	rows, err := db.Query(`SELECT id, warehouse_id, rice_id, amount, upd_date, activity FROM rice_packing_daily WHERE activity="packed" and id = ` + ID)
	if err != nil {
		return packing, err
	}
	for rows.Next() {
		packing := Rice_packing_daily{}
		rows.Scan(&packing.Id, &packing.Warehouse_id, &packing.Rice_id, &packing.Amount, &packing.Upd_date, &packing.Activity)
		return packing, err
	}
	return Rice_packing_daily{}, err
}

func DeletePacking(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	ID := prm.ByName("id")
	packing, err := NewPackingDaily().FindPackingByID(ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = packing.DeletePackings(ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (data *Rice_packing_daily) DeletePackings(ID string) error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	_, err = db.Exec(`DELETE FROM rice_packing_daily WHERE id = ?`, data.Id)
	if err != nil {
		return err
	}
	return nil
}

func UpdatePacking(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	ID := params.ByName("id")
	var packing Rice_packing_daily
	err := json.NewDecoder(req.Body).Decode(&packing)
	defer req.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if packing.Id == 0 {
		http.Error(w, "Please set ID in transfer information", http.StatusBadRequest)
		return
	}
	_, err = NewPackingDaily().FindPackingByID(ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = NewPackingDaily().UpdatePackingDaily(packing)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (data *Rice_packing_daily) UpdatePackingDaily(newPacking Rice_packing_daily) error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	_, err = db.Exec(`UPDATE rice_packing_daily SET warehouse_id = ?, rice_id = ? , amount = ?, upd_date = SYSDATE(), activity = "packed" WHERE id = ?`, newPacking.Warehouse_id, newPacking.Rice_id, newPacking.Amount, newPacking.Id)
	if err != nil {
		return err
	}
	return nil
}

func GetSaleByID(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	id := params.ByName("id")
	sale, err := NewRiceSale().FindSaleByID(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(sale)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (data *Rice_sale) FindSaleByID(ID string) (Rice_sale, error) {
	if ID == "" {
		return Rice_sale{}, errors.New("ID can not be empty")
	}
	db, err := GetDB()
	sale := Rice_sale{}
	if err != nil {
		return sale, err
	}
	defer db.Close()
	rows, err := db.Query(`SELECT id, warehouse_id, rice_id, amount, upd_date FROM rice_sale WHERE id = ` + ID)
	if err != nil {
		return sale, err
	}
	for rows.Next() {
		sale := Rice_sale{}
		rows.Scan(&sale.Id, &sale.Warehouse_id, &sale.Rice_id, &sale.Amount, &sale.Upd_date)
		return sale, err
	}
	return Rice_sale{}, err
}

func DeleteSale(w http.ResponseWriter, req *http.Request, prm httprouter.Params) {
	ID := prm.ByName("id")
	sale, err := NewRiceSale().FindSaleByID(ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = sale.DeleteSales(ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (data *Rice_sale) DeleteSales(ID string) error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	_, err = db.Exec(`DELETE FROM rice_sale WHERE id = ?`, data.Id)
	if err != nil {
		return err
	}
	return nil
}

func UpdateSale(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	ID := params.ByName("id")
	var sale Rice_sale
	err := json.NewDecoder(req.Body).Decode(&sale)
	defer req.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if sale.Id == 0 {
		http.Error(w, "Please set ID in transfer information", http.StatusBadRequest)
		return
	}
	_, err = NewRiceSale().FindSaleByID(ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = NewRiceSale().UpdateSales(sale)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (data *Rice_sale) UpdateSales(newSale Rice_sale) error {
	db, err := GetDB()
	if err != nil {
		return err
	}
	_, err = db.Exec(`UPDATE rice_sale SET warehouse_id = ?, rice_id = ? , amount = ?, upd_date = SYSDATE() WHERE id = ?`, newSale.Warehouse_id, newSale.Rice_id, newSale.Amount, newSale.Id)
	if err != nil {
		return err
	}
	return nil
}
